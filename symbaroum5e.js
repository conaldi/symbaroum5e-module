import ActorSheet5eCharacter from "./../../systems/dnd5e/module/actor/sheets/character.js";
import Actor5E from "./../../systems/dnd5e/module/actor/entity.js";
import {DND5E} from './../../systems/dnd5e/module/config.js';


Hooks.on('init', async function () {
	
	//DO CONFIG CHANGES

	CONFIG.DND5E.currencies = {
	  "gp": "SYM5E.CoinsGP",
	  "sp": "SYM5E.CoinsSP",
	  "cp": "SYM5E.CoinsCP",
	};	
	
	CONFIG.DND5E.languages = {
	  "common": "AIME.LanguagesCommon",
	  "blackspeech": "AIME.LanguagesBlackSpeech",
	  "ancient": "AIME.LanguagesQuenya",
	  "sindarin": "AIME.LanguagesSindarin",
	  "dalish": "AIME.LanguagesDalish",
	  "vale": "AIME.LanguagesVale",
	  "dwarvish": "AIME.LanguagesDwarvish",
	  "woodland": "AIME.LanguagesWoodland",
	  "rohan": "AIME.LanguagesRohan"
	};	

	// Remove PP and EP from showing up on character sheet displays since we don't use them in AiME	
  const originalGetData = game.dnd5e.applications.ActorSheet5eCharacter.prototype.getData;
  if (typeof libWrapper === "function") {
    libWrapper.register(
      "symbaroum5e",
      "game.dnd5e.applications.ActorSheet5eCharacter.prototype.getData",
      function (wrapper, ...args) {
        const sheetData = originalGetData.call(this);
        	delete data.data.currency.pp;
          delete data.data.currency.ep;
                wrapper.apply(this, args);
        return sheetData;
      },
      "WRAPPER"
    );
 } else {};

	// Test Fixes on Actor5E Prototype
	Actor5E.prototype.prepareDerivedData = function () {		
	
	console.log("ALAKAR!");
	const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags.dnd5e || {};
    const bonuses = getProperty(data, "bonuses.abilities") || {};

    // Retrieve data for polymorphed actors
    let originalSaves = null;
    let originalSkills = null;
    if (this.isPolymorphed) {
      const transformOptions = this.getFlag('dnd5e', 'transformOptions');
      const original = game.actors?.get(this.getFlag('dnd5e', 'originalActor'));
      if (original) {
        if (transformOptions.mergeSaves) {
          originalSaves = original.data.data.abilities;
        }
        if (transformOptions.mergeSkills) {
          originalSkills = original.data.data.skills;
        }
      }
    }

    // Ability modifiers and saves
    const saveBonus = Number.isNumeric(bonuses.save) ? parseInt(bonuses.save) : 0;
    const checkBonus = Number.isNumeric(bonuses.check) ? parseInt(bonuses.check) : 0;
    for (let [id, abl] of Object.entries(data.abilities)) {
      abl.mod = Math.floor((abl.value - 10) / 2);
      abl.prof = (abl.proficient || 0) * data.attributes.prof;
      abl.saveBonus = saveBonus;
      abl.checkBonus = checkBonus;
      abl.save = abl.mod + abl.prof + abl.saveBonus;

      // If we merged saves when transforming, take the highest bonus here.
      if (originalSaves && abl.proficient) {
        abl.save = Math.max(abl.save, originalSaves[id].save);
      }
    }
    
	this._prepareSkills(actorData, bonuses, checkBonus, originalSkills);

    // Determine Initiative Modifier
    const init = data.attributes.init;
    const athlete = flags.remarkableAthlete;
    const joat = flags.jackOfAllTrades;
    init.mod = data.abilities.dex.mod;
    if ( joat ) init.prof = Math.floor(0.5 * data.attributes.prof);
    else if ( athlete ) init.prof = Math.ceil(0.5 * data.attributes.prof);
    else init.prof = 0;
    init.bonus = init.value + (flags.initiativeAlert ? 5 : 0);
    init.total = init.mod + init.prof + init.bonus;

    // Prepare spell-casting data
    data.attributes.spelldc = this.getSpellDC(data.attributes.spellcasting);
    this._computeSpellcastingProgression(this.data);
  }

});
